class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  helper :all
  protect_from_forgery with: :exception
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :layout_by_resource
  before_filter :set_current_user

  protected

  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) do |u|
      u.permit(:name, :email, :password, :password_confirmation, :about)
    end
  end

  def layout_by_resource
    if devise_controller?
      'session_layout'
    else
      'application'
    end
  end

  def set_current_user
    Todo.current_user = current_user
  end
end

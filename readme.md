ToDO HUB is a simple todo application build using ruby on rails 4.

[![Code Climate](https://codeclimate.com/github/nidhinnambiar/ToDo-HUB/badges/gpa.svg)](https://codeclimate.com/github/nidhinnambiar/ToDo-HUB)
[![Build Status](https://travis-ci.org/nidhinnambiar/ToDo-HUB.svg?branch=master)](https://travis-ci.org/nidhinnambiar/ToDo-HUB)
[![bitHound Score](https://www.bithound.io/github/nidhinnambiar/ToDo-HUB/badges/score.svg)](https://www.bithound.io/github/nidhinnambiar/ToDo-HUB)

Simple todo app using rails 4

#Installation

* rvm install 2.2.2
* git clone https://github.com/nidhinnambiar/ToDo-HUB.git
* rake db:migrate
* bundle install
* rails s

#Check Out

You can find [here](https://todohub.herokuapp.com) the working model of ToDoHUB
